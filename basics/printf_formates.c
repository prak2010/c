/*
    Author: Prakash Ranjan
    Date: 15th July
    File: printf_formates.c 
*/

#include<stdio.h>
#include<stdlib.h>

int main(void){
    printf("\n\n================Hello World==================\n");
    printf("================Printf Formates================\n");
    
    printf("\n\nFirst example\n");
    printf("==============\n");
    
    int a,b;
    float c,d;

    a = 15;
    b = a / 2;
    printf("%d\n",b);
    printf("%3d\n",b);
    printf("%03d\n",b);

    c = 15.3;
    d = c / 3;
    printf("%3.2f\n\n",d);

    printf("\n\nSecond example\n");
    printf("================\n");

    int Fahrenheit;
    printf("Fahrenheit  Celcius\n");
    for (Fahrenheit = 0; Fahrenheit <= 300; Fahrenheit = Fahrenheit + 20){
        printf("%3d %15.3f\n", Fahrenheit, (5.0/9.0)*(Fahrenheit-32));
    }

    printf("\n\nThird example\n");
    printf("================\n");

    printf("The color: %s\n", "blue");
    printf("First number: %d\n", 12345);
    printf("Second number: %04d\n", 25);
    printf("Third number: %i\n", 1234);
    printf("Float number: %3.2f\n", 3.14159);
    printf("Hexadecimal: %X\n", 255);
    printf("Octal: %o\n", 255);
    printf("Unsigned value: %u\n", 150);
    printf("Just print the percentage sign %%\n", 10);

    printf("\n\nFourth example\n");
    printf("================\n");

    printf(":%s:\n", "Hello, world!");
    printf(":%15s:\n", "Hello, world!");
    printf(":%.10s:\n", "Hello, world!");
    printf(":%-15s:\n", "Hello, world!");
    printf(":%-15s:\n", "Hello, world!");
    printf(":%.15s:\n", "Hello, world!");
    printf(":%15.10s:\n", "Hello, world!");
    printf(":%-15.10s:\n", "Hello, world!");

    printf("================End of Printf Formates=========\n\n");
    return 0;
}

