/*
    Author: Prakash Ranjan
    Date:   16th July 2012
    File:   scanf_formates.c
*/

/* scanf declaration 
    int scanf(char *format, ...)  
*/

#include<stdio.h>

int main(void)
{
    printf("\n\n==============program starts here=================\n\n"); 
    
    int day, year;
    char monthname[20];
    char line[100] = "16 July 2012";
    char month[20];
    char c, eat_last_caracter;

    printf("\n========================\n    Example of sscanf\n========================\n\n");
    int ret = sscanf(line,"%d %s %d",&day,month,&year);
    printf("%d\n",ret);
    printf("%d\t %s\t %d\n\n",day,month,year);
    printf("\n========================\n    End of sscanf\n========================\n\n");

    printf("\n========================\nDifferent forms of input in scanf\n========================\n\n");
    printf("Intiger input\n");
    scanf("%d",&day);
    printf("%d\n",day);
    printf("Hexadecimal input\n");
    scanf("%X",&day);
    printf("%X\n",day);
    printf("Character input\n");
    scanf("%c\n",&c);
    printf("%c\n",c);
    scanf("%c",&eat_last_caracter); /* Used to eat the last stdin \n character so that 
                                       we can enter the string in next section */
    printf("String input\n");
    scanf("%s",month);
    printf("%s\n",month);

    printf("\n\n==============program Ends here===================\n\n"); 
    return 0;
}

