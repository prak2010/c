/*
    Author: Prakash Ranjan
    Date: 16th July 2012
    File: little_big_endian.c
          check the machine is little endian or big endian
*/

#include<stdio.h>
int main(void){
    char a = 1;
    char b = 0;
    printf("a = %d;\t b = %d\n",a,b);
    b = a & 0X01;
    if(b == 1){
        printf("The machine is little endian\n");
    }else{
        printf("The machine is big endian\n");
    }
    return 0;
}


