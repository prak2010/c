#include<stdio.h>
#include<signal.h>

void signal_handler(int signal)
{
    printf("In the signal handler\n");

    printf("The value of signal = %d\n", signal);
    return ;
}

int main(void)
{
    
    int i =1;

    signal(SIGUSR1,signal_handler);

    while(i);

    return 0;
}
