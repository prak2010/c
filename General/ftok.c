#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<sys/syscall.h>
#include<fcntl.h>
#include<sys/types.h>



void generate_identity(pid_t pid, pid_t tid)
{
    char path[100];

    sprintf(path,"%s%d_%d","./",pid,tid);
    FILE *fp = fopen(path,"w+");
    if(fp == NULL){
        printf("Error in opening the file\n");
    }

    int ret =ftok(path,1);
    printf("ret = %d\n",ret);
    
}


void * thread_func(void * ptr)
{
    pid_t pid, tid;


    pid  = getpid();

    printf("The value of pid %d\n", pid);

    tid  = syscall(SYS_gettid);

//    tid = gettid();
    printf("The value of tid %d\n", tid);
    
    generate_identity(pid, tid);

   return NULL; 
}


int main(void)
{
    pthread_t pid;
    int ret;
    
    ret = pthread_create(&pid, NULL, thread_func, NULL);
    if(ret < 0){
        printf("Error in creating the thread \n");
    }
    
    sleep(2);
    return 0;
}
