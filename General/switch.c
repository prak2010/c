#include<stdio.h>

int main(void)
{
    float a;
    scanf("%f", &a);

    switch (a){
        case 'A':
                printf("The switch case can work on A characters\n");
                break;
        case 'B':
                printf("The switch case can work on B characters\n");
                break;

        default:
                printf("The switch is not working on characters\n");
     }
    return 0;
}
            
    
